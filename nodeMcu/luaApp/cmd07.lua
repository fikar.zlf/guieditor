--
-- Stop pwm at ESP8266 pin
--
local retCmd = {}
local function doCmd(sm, sck)    
  dofile("drawCmdBase.lua")(7)
  local str = "Stop pwm out"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 28, str);
  str = "Pin nro:" .. sm["data"][1]
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 44, str);
  disp:sendBuffer()
  pwm.stop(sm["data"][1])
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
