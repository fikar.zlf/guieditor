--
--  Setup inpout for adc (can be Pin D0 or vcc)
--  Setting the source to vcc require restart
-- 
local retCmd = {}
local function doCmd(sm, sck)    
  dofile("drawCmdBase.lua")(8)
  local str = "Setupd ADC in"
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 28, str)

  if(sm["data"][2] == 0) then
     str = "Source: pin D0"
  else
    str = "Source: vcc" 
  end
  x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 44, str);
  disp:sendBuffer()

  if(sm["data"][2] == 0) then
    if adc.force_init_mode(adc.INIT_ADC) then
      node.restart();
    end
  else
    if adc.force_init_mode(adc.INIT_VDD33) then
      node.restart();
    end
  end
end
retCmd.doCmd = doCmd;
collectgarbage()
return retCmd;
