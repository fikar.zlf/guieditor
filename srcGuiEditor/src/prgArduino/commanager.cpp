/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "commanager.h"
#include <string.h>
#include <QMessageBox>
#include "actsync.h"
#include "actsignature.h"
#include "actsoftversion.h"
#include "actsetdevparam.h"
#include "actenableprogram.h"
#include "actcheckmem.h"
#include "actwritemem.h"
#include "actleaveprg.h"

void comManager::addCmd(serialCmd *cmd)
{
    cmdList.enqueue(cmd);
    if(state == stIdle)
        executeCmd();
}

void comManager::addAction(prgAction *act)
{
    actionList.enqueue(act);
    if(state == stIdle)
      executeAction();
}

void comManager::executeCmd(void)
{
    if(serPort->isOpen())
    {
        state = stRunning;
        serPort->write(cmdList.head()->dataToSend());
        serPort->flush();
        timeOut.start(250);
    }
}

void comManager::executeAction(void)
{
    QVector<serialCmd *> cmd;
    int i;
    actionList.head()->getSerialCmd(cmd);
    for(i=0; i < cmd.length(); i++)
        cmdList.enqueue(cmd[i]);
    if(state == stIdle)
        executeCmd();
}

void comManager::dataReady()
{
    QByteArray recivedData;
    serialCmd * cmd;
    prgAction * act;
    timeOut.stop();
    while(serPort->bytesAvailable() > 0)
      recivedData = recivedData + serPort->readAll();    

    if(cmdList.count() > 0)
        switch(cmdList.head()->processResponse(recivedData))
        {
        case rcvOk:
            cmd = cmdList.dequeue();
            emit updateStateCmd(cmd->cmdInfo() + ": Ok");
            if(cmd->isLastCmd())
            {   /* Es el último -> agregamos una nueva acción */
                this->execActCnt++;
                emit updateStateAct(cmd->getAction()->info());
                emit updateStateAct(this->execActCnt, this->totalActCnt);
                act = actionList.dequeue();
                delete act;
                state = stIdle;
                if(actionList.count() > 0)
                    executeAction();
            }
            else if(cmdList.head()->mustSkip())
            {   /* Se debe omitir el resto -> agregamos una nueva acción */
                this->execActCnt++;
                emit updateStateAct(cmd->getAction()->info());
                emit updateStateAct(this->execActCnt, this->totalActCnt);
                cmdList.clear();
                act = actionList.dequeue();
                delete act;
                state = stIdle;
                if(actionList.count() > 0)
                    executeAction();
            }
            else if(cmdList.count() > 0)
                executeCmd(); /* Debemos continuar */
            break;
        case rcvFail:
            cmd = cmdList.dequeue();
            emit updateStateCmd(cmd->cmdInfo() + ": fail");
            emit updateStateAct(cmd->getAction()->info());
            emit programError(cmd->getAction()->info());
            cmdList.clear();
            while(actionList.count()>0)
            {
                act = actionList.dequeue();
                delete act;
            }
            state = stIdle;
            break;
        case rcvWait:
            break;
        }
    else
    {
        emit updateStateCmd("Warning: recived data with out commmand");
    }
}


void comManager::onTimeOut(void)
{
    timeOut.stop();
    prgAction * act;
    serialCmd * cmd = cmdList.head();
    cmd->addRetry();
    if(cmd->getRetry() >= serialCmd::maxRetry)
    {
        cmdList.clear();
        while(actionList.count()>0)
        {
          act = actionList.dequeue();
          delete act;
        }
        emit updateStateCmd("Timeout!");
        emit programError(tr("\nDon't have response from board when initialize the programming procedure"));
        state = stIdle;
    }
    else
    {
        emit updateStateCmd("Retry...");
        executeCmd();
    }
}

void comManager::resetStart(void)
{
    serPort->setRequestToSend(true);
    serPort->setDataTerminalReady(true);
    QTimer::singleShot(10,this, SLOT(resetEnd()));
}

void comManager::resetEnd(void)
{
  uint i;
  addAction(new actSync());
  addAction(new actSignature());
  addAction(new actSoftVersion());
  addAction(new actSetDevParam());
  addAction(new actEnableProgram());
  this->totalActCnt = 5;
  for(i = 0; i < fw.pageCount(); i++)
  {
      addAction(new actWriteMem(fw.getPage(i)));
      this->totalActCnt++;
  }
  for(i = 0; i < fw.pageCount(); i++)
  {
      addAction(new actCheckMem(fw.getPage(i)));
      this->totalActCnt++;
  }
  addAction(new actLeavePrg());
  this->totalActCnt++;
}

comManager::comManager(QObject *parent) :
    QObject(parent)
{
  serPort = new QSerialPort(this);
  state = stIdle;  
  connect(&timeOut, SIGNAL(timeout()), this, SLOT(onTimeOut()));
}

comManager::~comManager(void)
{  
  timeOut.stop();
  if(serPort->isOpen())
      serPort->close();  
  delete serPort;  
}

bool comManager::openDevice(QString port, QSerialPort::BaudRate baud)
{
    bool ret = false;
    serialCmd * cmd;
    if(serPort->isOpen())
        serPort->close();
    while(!cmdList.empty())
    {
        cmd = cmdList.dequeue();
        delete cmd;
    }
    serPort->setPortName(port);
    serPort->setBaudRate(baud); // 115200 BAUD57600
    serPort->setDataBits(QSerialPort::Data8);
    serPort->setFlowControl(QSerialPort::NoFlowControl);
    serPort->setParity(QSerialPort::NoParity);
    serPort->setStopBits(QSerialPort::OneStop);
    serPort->open(QIODevice::ReadWrite);        
    ret = serPort->isOpen();
    if(ret)
        connect(serPort, SIGNAL(readyRead()), this, SLOT(dataReady()));
    return ret;
}

void comManager::closeDevice(void)
{
    disconnect(serPort, SIGNAL(readyRead()), this, SLOT(dataReady()));
    serPort->close();
    state = stIdle;
    while(actionList.count()>0)
    {
        delete actionList.dequeue();
    }
}

bool comManager::isConnected(void)
{
    return serPort->isOpen();
}



void comManager::start(QString FileName)
{
    this->totalActCnt = 0;
    this->execActCnt = 0;
    this->fwFileName = FileName;

    this->fw.process(this->fwFileName);
    this->fw.paginate(128);

    serPort->setRequestToSend(false);
    serPort->setDataTerminalReady(false);
    emit updateStateAct(QString("Starting..."));
    emit updateStateCmd(QString("Starting..."));
    QTimer::singleShot(250,this, SLOT(resetStart()));
}

