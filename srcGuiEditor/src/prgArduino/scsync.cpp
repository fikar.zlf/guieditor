/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "scsync.h"
#include "QByteArray"

scSync::scSync(prgAction *act) :
    serialCmd(act, true)
{
    buf.clear();
    this->retryCount = 0;
}

QByteArray scSync::dataToSend(void)
{
    QByteArray toSend;
    toSend.push_back(Cmnd_STK_GET_SYNC);
    toSend.push_back(Sync_CRC_EOP);
    return toSend;
}

QString scSync::cmdInfo(){
    return "Sync";
}

rcvType scSync::processResponse(QByteArray data)
{
    rcvType ret = rcvWait;
    buf.push_back(data);
    if ((buf.length() == 2) && (buf[0] == Resp_STK_INSYNC) && (buf[1] == Resp_STK_OK))
        ret = rcvOk;
    else if((buf.length() == 1) && (buf[0] == Resp_STK_NOSYNC))
        ret = rcvFail;
    return ret;
}

scSync::~scSync(void)
{

}
