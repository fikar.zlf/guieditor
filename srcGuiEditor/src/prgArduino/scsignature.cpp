/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "scsignature.h"
#include "QByteArray"

scSignature::scSignature(prgAction *act) :
    serialCmd(act, true)
{
    buf.clear();
}

QByteArray scSignature::dataToSend(void)
{
    QByteArray toSend;
    toSend.push_back(Cmnd_STK_READ_SIGN);
    toSend.push_back(Sync_CRC_EOP);
    return toSend;
}

QString scSignature::cmdInfo(){
    return "Signature validation";
}

rcvType scSignature::processResponse(QByteArray data)
{
    rcvType ret = rcvWait;
    buf.push_back(data);
    if ((buf.length() == 5) && (buf[0] == Resp_STK_INSYNC) && (buf[4] == Resp_STK_OK))
    {
        if((buf[1] == ((char)0x1E)) && (buf[2] == ((char)0x95)) && (buf[3] == ((char)0x0F)))
        {
            retVal = (0x1E << 16) | (0x95 << 8) | 0x0F;
          ret = rcvOk;
        }
        else
          ret = rcvFail;
    }
    else if((buf.length() == 1) && (buf[0] == Resp_STK_NOSYNC))
        ret = rcvFail;
    return ret;
}

scSignature::~scSignature(void)
{

}
