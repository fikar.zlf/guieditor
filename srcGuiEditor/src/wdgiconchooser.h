#ifndef WDGICONCHOOSER_H
#define WDGICONCHOOSER_H

#include <QWidget>
#include "abstractuicctrl.h"
namespace Ui {
class wdgIconChooser;
}

class wdgIconChooser : public QWidget, public updPropWdg
{
    Q_OBJECT

public:
    explicit wdgIconChooser(abstractPropEditor * prop);
    ~wdgIconChooser();
    void setIconFileName(QString);
    QString getIconFileName(void);
virtual void updPropValue();
private slots:
    void on_btnChangeIcon_clicked();

private:
    QStringList imgList;
    Ui::wdgIconChooser *ui;
    QString iconFileName;
    QString imgPath;
};

#endif // WDGICONCHOOSER_H
