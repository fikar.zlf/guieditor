/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef CHECKBOXCTRL_H
#define CHECKBOXCTRL_H
#include <QWidget>
#include "abstractuicctrl.h"
#include <QCheckBox>
#include <QFontMetrics>
#include "commonproperties.h"
#include "controlgenerator.h"




class cbCheckPropEditor: public abstractPropEditor
{
    Q_OBJECT
public:
    cbCheckPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void cbCheckedChanged(QString text);
};


class cbTextPropEditor: public abstractPropEditor
{
public:
    cbTextPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl){
        this->propName = "String";
    }
    QString getValue(){
        return ((QCheckBox *)this->ctrl->associatedWidget())->text();
    }

    bool isValidValue(QString newVal){newVal = ""; return true;}
    virtual void setValue(QString newVal)
    {        
        //QFontMetrics fm(((QCheckBox *)this->ctrl->associatedWidget())->font());
        ((QCheckBox *)this->ctrl->associatedWidget())->setText(newVal);
        //((QCheckBox *)this->ctrl->associatedWidget())->resize(fm.width(newVal), fm.height());

    }

    virtual QString generateCode()
    {
        return "'" + this->propName + "', '" + this->getValue() + "'";
    }
};

class cbBgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    cbBgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class cbFgClrPropEditor: public abstractPropEditor
{
 Q_OBJECT
private:
    QPalette palette;
public:
    cbFgClrPropEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void valueChooserChanged();
};

class checkBoxCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;

public:

    static unsigned int getNameCounter();
    QStringList createCallBack(QString path);
    checkBoxCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg *parentWnd);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    virtual QString className() { return "checkBoxCtrl";}
    virtual bool isAutoSize();
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool);
    virtual ~checkBoxCtrl();
};

class cbCtrlGen: public controlGenerator
{
public:
  cbCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // CHECKBOXCTRL_H


