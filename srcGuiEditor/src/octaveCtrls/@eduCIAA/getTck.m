function ret = getTck(obj, val)
  v = [176, 1, 2, val, 0, 11];

  crcPos =  length(v)-1;
  v(crcPos) = 0;
  for i=1:crcPos - 1
    v(crcPos) = v(crcPos) + v(i);
  endfor
  v(crcPos) = mod(v(crcPos), 256);      
  
  port = obj.serialDev;
  srl_flush(port);
  for i=1:length(v)
    srl_write(port, uint8(v(i)));
  endfor
  ret = uint8(srl_read(port, 1));  
endfunction
