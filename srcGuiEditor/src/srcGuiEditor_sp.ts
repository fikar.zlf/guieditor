<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_AR">
<context>
    <name>QObject</name>
    <message>
        <location filename="configsettings.cpp" line="259"/>
        <source>Configuration changed</source>
        <translation>Cambio de configuración</translation>
    </message>
    <message>
        <location filename="configsettings.cpp" line="259"/>
        <source>To apply the selected fonts, guiEditor must be restart. Please, save your work and restart it.</source>
        <translation>Para aplicar el tipo de letra seleccionado, guiEditor debe reiniciarse. Por favor, guarde su trabajo y reinícielo.</translation>
    </message>
    <message>
        <location filename="configsettings.cpp" line="264"/>
        <location filename="configsettings.cpp" line="270"/>
        <source>Cambio de configuración</source>
        <translation></translation>
    </message>
    <message>
        <location filename="configsettings.cpp" line="264"/>
        <location filename="configsettings.cpp" line="270"/>
        <source>El idioma será actualizado la próxima vez que inicie la aplicación</source>
        <translation></translation>
    </message>
    <message>
        <source>% This definition is for backward compatibility and can be remove in next versions.</source>
        <translation type="vanished">% Esta definición se ha agregado para mantener compatibilidad con versiones previas y será removida en las proximas versiones.</translation>
    </message>
    <message>
        <source>% you must reference controls over the window using the name of dialog window.</source>
        <translation type="vanished">% Usted debería referenciar los controles sobre la ventana a través del nombre de la ventana que lo contiene.</translation>
    </message>
    <message>
        <location filename="buttonctrl.cpp" line="218"/>
        <source>% This code will be executed when user click the button control.</source>
        <translation>% El código que se indique aquí será ejecutado cuando el usuario presione en el botón.</translation>
    </message>
    <message>
        <location filename="buttonctrl.cpp" line="219"/>
        <location filename="editctrl.cpp" line="276"/>
        <location filename="sliderctrl.cpp" line="202"/>
        <source>% As default, all events are deactivated, to activate must set the</source>
        <translation>% Por defecto, todos los eventos están desactivados, para activarlos debe marcar</translation>
    </message>
    <message>
        <location filename="buttonctrl.cpp" line="220"/>
        <source>% property &apos;generateCallback&apos; from the properties editor</source>
        <oldsource>% propertie &apos;generateCallback&apos; from the properties editor</oldsource>
        <translation>% la propiedad &apos;generateCallback&apos; desde el editor de propiedades</translation>
    </message>
    <message>
        <location filename="editctrl.cpp" line="274"/>
        <source>% This code will be executed when the control lost focus and text has </source>
        <translation>% Este código será ejecutado cuando el control pierda el foco y el texto</translation>
    </message>
    <message>
        <location filename="editctrl.cpp" line="275"/>
        <source>% been changed or when press the &quot;enter&quot; key.</source>
        <translation>% haya sido modificado o cuando presione la tecla enter</translation>
    </message>
    <message>
        <location filename="editctrl.cpp" line="277"/>
        <source>% property &apos;have callback&apos; from the properties editor</source>
        <oldsource>% propertie &apos;have callback&apos; from the properties editor</oldsource>
        <translation>% la propiedad generateCallback desde el editor de propiedades</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="577"/>
        <source>Export project</source>
        <translation>Exportar proyecto</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="577"/>
        <source>The project was exported to folder </source>
        <translation>El proyecto ha sido exportado a la carpeta </translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="653"/>
        <source>Not defined a main dialog</source>
        <translation>No se ha definido un diálogo inicial</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="653"/>
        <source>To run a project, you must define a main dialog. This will be the first dialog showed. If there is no dialog in project, add one. If have some dialog, mark as main dialog (left click over them in the project mannager, and select &quot;set as main dialog&quot;)</source>
        <translation>Para ejecutar un proyecto, usted debe definir cuál es la ventana principal asociada. Esta será la ventana que se mostrará cuando el proyecto inicie. Si no hay ninguna ventana de diálogo en el proyecto activo, agregue una. Si existe alguna, máquela como ventana de diálogo inicial (botón derecho en una ventana de diálogo y eleccione &quot;Establecer como ventana inicial&quot;)</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="676"/>
        <location filename="guiproject.cpp" line="711"/>
        <source>Generate octave package</source>
        <translation>Generar paquete</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="677"/>
        <source>To generate a package from this project, you must configure the local path to gzip and tar program from File/Properties menu. This applications can be found in /bin or /usr/bin folder in GNU Linux or must be installed as external program in MS Windows.</source>
        <translation>Para generar un paquete a partir de este proyecto, debe configurar la ruta donde pueden encontrarse las aplicaciones gzip y tar desde la opción Archivo/Propiedades del menú. Estas aplicaciones pueden encontrarse en las carpetas /bin o /usr/bin en varias distribuciones de GNU Linux o deben instalarse como aplicaciones externas en el caso del sistema operativo MS Windows.</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="712"/>
        <source>A octave package must have special texts files with license information, details about of package and other data. Not all files are finded in project, basic versions of them are created and added.</source>
        <translation>Un paquete de octave tiene archivos de texto con información sobre la licencia, detalles del paquete y otros datos. No todos los archivos han sido encontrados en el proyecto, por lo que una versión básica de ellos se creará y agregará al proyecto. Por favor, revise su contenido.</translation>
    </message>
    <message>
        <source>Not implemente</source>
        <translation type="vanished">No implementado</translation>
    </message>
    <message>
        <source>Ups.. we are working on it!</source>
        <translation type="vanished">Ups... estamos trabajando en esto. Disculpe por favor!</translation>
    </message>
    <message>
        <location filename="callbackctrl.cpp" line="52"/>
        <source>% This code can define a callback.</source>
        <translation>% Este código puede definir un callback.</translation>
    </message>
    <message>
        <location filename="callbackctrl.cpp" line="53"/>
        <source>% You must associate them to figure or control onload like event window</source>
        <translation>% Debe asociarlo a una figura o control en eventos del tipo &quot;onload&quot; asociados a la ventana de diálogo</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="114"/>
        <location filename="childwndimg.cpp" line="62"/>
        <location filename="childwndmfile.cpp" line="64"/>
        <location filename="childwndtextfile.cpp" line="61"/>
        <source>Warning!!! This operation cannot be undone</source>
        <translation>Cuidado!!!! Esta operación no puede ser deshecha</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="115"/>
        <location filename="childwndimg.cpp" line="63"/>
        <location filename="childwndmfile.cpp" line="65"/>
        <location filename="childwndtextfile.cpp" line="62"/>
        <source>You are trying to delete the file %1, this operation can&apos;t be undone. Are you sure to delete this file?</source>
        <translation>Usted está intentando eliminar el archivo %1, esta operación no puede deshacerse. Está seguro que desea eliminar el archivo?</translation>
    </message>
    <message>
        <location filename="sliderctrl.cpp" line="201"/>
        <source>% This code will be executed when user change the value of slider.</source>
        <translation>% Este código será ejecutado cuando el usuario cambie el valor de la barra.</translation>
    </message>
    <message>
        <location filename="sliderctrl.cpp" line="203"/>
        <source>% property &apos;generateCallbck&apos; from the properties editor</source>
        <oldsource>% propertie &apos;generateCallbck&apos; from the properties editor</oldsource>
        <translation>% propiedad &apos;generateCallback&apos; desde el editor de propiedades</translation>
    </message>
</context>
<context>
    <name>QextSerialPort</name>
    <message>
        <source>No Error has occurred</source>
        <translation type="vanished">No se han producido errores</translation>
    </message>
    <message>
        <source>Invalid file descriptor (port was not opened correctly)</source>
        <translation type="vanished">Descriptor de archivos inválido (el puerto no ha sido abierto de modo correcto)</translation>
    </message>
    <message>
        <source>Unable to allocate memory tables (POSIX)</source>
        <translation type="vanished">No ha sido posible alojar memoria (POSIX)</translation>
    </message>
    <message>
        <source>Caught a non-blocked signal (POSIX)</source>
        <translation type="vanished">Se capturó una señal no bloqueada</translation>
    </message>
    <message>
        <source>Operation timed out (POSIX)</source>
        <translation type="vanished">Tiempo de espera agotado (POSIX)</translation>
    </message>
    <message>
        <source>The file opened by the port is not a valid device</source>
        <translation type="vanished">El archivo abierto por el puerto no es un dispositivo válido</translation>
    </message>
    <message>
        <source>The port detected a break condition</source>
        <translation type="vanished">El puerto ha detectado una condición de interrupción (break)</translation>
    </message>
    <message>
        <source>The port detected a framing error (usually caused by incorrect baud rate settings)</source>
        <translation type="vanished">El puerto detectó un error de &apos;framing&apos; (usualmente esto se debe a una configuración incorrecta de la velocidad de transmición)</translation>
    </message>
    <message>
        <source>There was an I/O error while communicating with the port</source>
        <translation type="vanished">Se ha producido un error de entrada/salida miestras se estaba comunicando con el puerto</translation>
    </message>
    <message>
        <source>Character buffer overrun</source>
        <translation type="vanished">Se ha superador la capadidad del buffer</translation>
    </message>
    <message>
        <source>Receive buffer overflow</source>
        <translation type="vanished">Se ha superado el tamaño del buffer de recepsión</translation>
    </message>
    <message>
        <source>The port detected a parity error in the received data</source>
        <translation type="vanished">El puerto ha detectado un error de paridad en los datos recividos</translation>
    </message>
    <message>
        <source>Transmit buffer overflow</source>
        <translation type="vanished">Se ha superado el espacio de almacenamiento del buffer de trasmisicion</translation>
    </message>
    <message>
        <source>General read operation failure</source>
        <translation type="vanished">Falla general durante una operacion de lectura</translation>
    </message>
    <message>
        <source>General write operation failure</source>
        <translation type="vanished">Falla general durante una operacion de escritura</translation>
    </message>
    <message>
        <source>The %1 file doesn&apos;t exists</source>
        <translation type="vanished">El archivo %1 no existe</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Permiso denegado</translation>
    </message>
    <message>
        <source>Device is already locked</source>
        <translation type="vanished">El dispositivo se encuentra bloqueado</translation>
    </message>
    <message>
        <source>Unknown error: %1</source>
        <translation type="vanished">Error desconocido: %1</translation>
    </message>
</context>
<context>
    <name>aboutDlg</name>
    <message>
        <location filename="aboutdlg.ui" line="14"/>
        <source>About GUIEditor...</source>
        <translation>Sobre guiEditor...</translation>
    </message>
    <message>
        <location filename="aboutdlg.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;GUI Editor for Octave&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;guiEditor para GNU Octave&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutdlg.ui" line="33"/>
        <source>version</source>
        <translation>versión</translation>
    </message>
    <message>
        <location filename="aboutdlg.ui" line="43"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Copyright 2015-2020, Enrique Sergio Burgos&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor for Octave is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;You should have received a copy of the GNU General Public License along with GUI Editor for Octave. If not, see &amp;lt;http://www.gnu.org/licenses/&amp;gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Copyright 2015-2021, Enrique Sergio Burgos&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;guiEditor para GNU Octave es una aplicación gratuita: Usted puede redistribuirla y/o modificarla bajo los términos de la licencia pública GNU versión 3 o alguna posterior (GNU GPL V3).&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;guiEditor ha sido creado con la intensión que sea de su utilidad, pero sin ninguna garantía. Vea la licencia pública GNUpara más detalles.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Usted ha recibido una copia de la licencia GNU GPL junto a GUI Editor. Si este no fuera el caso, podrá encontrar una copia en  &amp;lt;http://www.gnu.org/licenses/&amp;gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Copyright 2015-2017, Enrique Sergio Burgos&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor for Octave is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;You should have received a copy of the GNU General Public License along with GUI Editor for Octave. If not, see &amp;lt;http://www.gnu.org/licenses/&amp;gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is free and open source application developed as part of a investigation project in the National Technological University, Regional Faculty Parana at Entre Rios (country of Argentina).&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Project group:&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;- Sergio Burgos (e.sergio.burgos@gmail.com) main developper of GUIEditor.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;- Eduardo J. Adam (eadam.fiq@gmail.com) and Francisco Sala (franciscosala@gmail.com) octave simulations developpers.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;- Omar Berardi (oberardi@gigared.com) and Fernando Sato (fsatopna@gmail.com) software development team.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;- Sebastian Vicario (sebastian_vicario@hotmail.com), Sergio Panoni (sergiohectorpanoni@yahoo.com.ar) and Hector Ramos (hectorramos@frp.utn.edu.ar) hardware developpers.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Copyright 2015-2020, Enrique Sergio Burgos&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor para Octave es una aplicación gratuita: Usted puede redistribuirla y/o modificarla bajo los términos de la licencia pública GNU versión 3 o alguna posterior (GNU GPL V3).&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;GUI Editor ha sido creado con la intensión que sea de su utilidad, pero sin ninguna garantía. Vea la licencia pública GNUpara más detalles.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Usted ha recibido una copia de la licencia GNU GPL junto a GUI Editor. Si este no fuera el caso, podrá encontrar una copia en  &amp;lt;http://www.gnu.org/licenses/&amp;gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutdlg.ui" line="68"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
</context>
<context>
    <name>alignWndClass</name>
    <message>
        <source>Align dialog</source>
        <translation type="vanished">Alineación</translation>
    </message>
    <message>
        <source>Align:</source>
        <translation type="vanished">Alineación:</translation>
    </message>
    <message>
        <source>Align respect to dialog:</source>
        <translation type="vanished">Alineación a la ventana:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="vanished">Tamaño:</translation>
    </message>
</context>
<context>
    <name>ardUnoPrgWnd</name>
    <message>
        <location filename="ardunoprgwnd.ui" line="14"/>
        <source>Arduino board program firmware</source>
        <translation>Programar firmware en placa arduino</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="26"/>
        <source>Arduino program firmware</source>
        <translation>Programar nuevo firmware en Arduino</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="36"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Using this option you can program a firmware to use a arduino board. At this moment, only arduino boards based on ATMega328 microcontroller are supported. &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;The firmware to program are included in GUIEditor and is selected from the option selected in Property Editor. The associated class to comunicate with board are generated automacally.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: the previus firmware will be erase&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Utilizando esta opción usted puede programar el firmware a utilizar en una placa Arduino. En este momento, solo placas arduino basada en el microcontrolador ATMega328 son soportadas.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;El firmware que se programará, está incluído en GUIEditor, el código fuente del mismo puede ser encontrado en la carpeta &quot;run/firmware&quot;. La clase necesaria para establecer comunicación desde octave se generará de modo automático.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Cuidado: cualquier firmware pre existente se eliminará al reprogramar la placa.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="61"/>
        <source>Serial port:</source>
        <translation>Puerto serie:</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="112"/>
        <source>Communication speed:</source>
        <translation>Velocidad de comunicación:</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="120"/>
        <source>115200</source>
        <translation>115200</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="125"/>
        <source>57600</source>
        <translation>57600</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="179"/>
        <source>Programming %p%</source>
        <translation>Programando %p %</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="150"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.ui" line="199"/>
        <source>Program</source>
        <translation>Programar</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="31"/>
        <source>Successful programming</source>
        <translation>Programación exitosa</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="31"/>
        <source>The programming process has been successfully completed</source>
        <translation>El proceso de programación ha sido completado exitósamente</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="37"/>
        <source>Programming error</source>
        <translation>Error durante la programación</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="37"/>
        <source>Something failed during the programming process. The action associated with the fault has returned the value:</source>
        <translation>Algo ha fallado durante el proceso de programación. La acción que ha fallado a retornado el valor:</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="85"/>
        <source>Serial port error</source>
        <translation>Error en el puerto serie</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="85"/>
        <source>The selected serial port</source>
        <translation>El puerto serie seleccionado</translation>
    </message>
    <message>
        <location filename="ardunoprgwnd.cpp" line="85"/>
        <source>could not open.</source>
        <translation>no se ha podido abrir.</translation>
    </message>
</context>
<context>
    <name>childWndDlg</name>
    <message>
        <location filename="childwnddlg.cpp" line="148"/>
        <source>Se perderá información</source>
        <translation>Es posible que se pierda informacion</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="149"/>
        <source>Las modificaciones realizadas no se han guardado. Desea salir de todos modos?</source>
        <translation>Las modificaciones realizadas no se han guardado. Desea salir de todos modos?</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="221"/>
        <source>Align left</source>
        <translation>Alinear a la izquierda</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="222"/>
        <source>H center</source>
        <translation>Centrar horizontalmente</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="223"/>
        <source>Aling right</source>
        <translation>Alinear a la derecha</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="227"/>
        <source>Align top</source>
        <translation>Alinear arriba</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="228"/>
        <source>V center</source>
        <translation>Centrar verticalmente</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="229"/>
        <source>Align bottom</source>
        <translation>Alinear abajo</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="233"/>
        <source>Center in dialog (H)</source>
        <translation>Centrar en la ventana (Horizontal)</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="234"/>
        <source>Center in dialog (V)</source>
        <translation>Centrar en la ventana (Vertical)</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="238"/>
        <source>Resize to highest</source>
        <translation>Redimensionar el más grande</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="239"/>
        <location filename="childwnddlg.cpp" line="244"/>
        <source>Resize to small</source>
        <translation>Redimensionar al más pequeño</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="243"/>
        <source>Resize to large</source>
        <translation>Redimensionar al más largo</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="250"/>
        <source>Move to:</source>
        <translation>Mover a:</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="310"/>
        <location filename="childwnddlg.cpp" line="331"/>
        <source>Select destination file name</source>
        <translation>Seleccione el archivo de destino</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="310"/>
        <source>XML Dialogs (*.dlg);;All files (*.*);;</source>
        <translation>Diálogos XML(*.xml);;Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="331"/>
        <source>XML Dialogs (*.xml);;All files (*);;</source>
        <translation>Diálodos XML (*.xml);;Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="962"/>
        <source>Warning!</source>
        <translation>Cuidado!</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="962"/>
        <source>You cannot move a control inside itself!</source>
        <translation>Usted no puede mover un control dentro de él mismo!</translation>
    </message>
</context>
<context>
    <name>childWndMFile</name>
    <message>
        <location filename="childwndmfile.cpp" line="114"/>
        <location filename="childwndmfile.cpp" line="157"/>
        <source>Select destination file name</source>
        <translation>Seleccione el archivo de destino</translation>
    </message>
    <message>
        <location filename="childwndmfile.cpp" line="114"/>
        <source>Octave script (*.m);;All files (*.*);;</source>
        <translation>Script de Octave (*.m);; Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="childwndmfile.cpp" line="157"/>
        <source>Octave script (*.m);;All files (*);;</source>
        <translation>Script de Octave (*.m);; Todos los archivos (*.*);;</translation>
    </message>
</context>
<context>
    <name>childWndTextFile</name>
    <message>
        <location filename="childwndtextfile.cpp" line="97"/>
        <location filename="childwndtextfile.cpp" line="134"/>
        <source>Select destination file name</source>
        <translation>Seleccione el archivo de destino</translation>
    </message>
    <message>
        <location filename="childwndtextfile.cpp" line="97"/>
        <source>Text files (*.txt);;All files (*.*);;</source>
        <translation>Archivos de texto (*.txt);; Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="childwndtextfile.cpp" line="134"/>
        <source>Text files (*.txt);;All files (*);;</source>
        <translation>Archivos de texto (*.txt);; Todos los archivos (*);;</translation>
    </message>
</context>
<context>
    <name>comManager</name>
    <message>
        <location filename="prgArduino/commanager.cpp" line="146"/>
        <source>
Don&apos;t have response from board when initialize the programming procedure</source>
        <translation>No se ha obtenido respuesta desde la placa cuando se ha iniciado el proceso de programación</translation>
    </message>
</context>
<context>
    <name>confirmWnd</name>
    <message>
        <location filename="confirmwnd.ui" line="14"/>
        <source>Exit?</source>
        <translation>Desea salir?</translation>
    </message>
    <message>
        <location filename="confirmwnd.ui" line="20"/>
        <source>Some documents are not saved. You want:
- Don&apos;t save any document and exit.
- Cancel and return to the main application.</source>
        <translation>Algunos documentos no se han salvado. Usted puede:
- Salir sin guardar nada.
- Cancelar y volver a la aplicación.</translation>
    </message>
    <message>
        <location filename="confirmwnd.ui" line="31"/>
        <source>Don&apos;t save and close</source>
        <translation>Salir sin guardar</translation>
    </message>
    <message>
        <location filename="confirmwnd.ui" line="51"/>
        <source>Cancel and return</source>
        <translation>Cancelar y volver</translation>
    </message>
</context>
<context>
    <name>debugWndClass</name>
    <message>
        <source>Output window</source>
        <translation type="vanished">Consola de Octave</translation>
    </message>
    <message>
        <source>Octave output:</source>
        <translation type="vanished">Salida de octave:</translation>
    </message>
    <message>
        <source>Clear output console</source>
        <translation type="vanished">Borrar la salida de consola</translation>
    </message>
    <message>
        <source>Copy selected lines from output console</source>
        <translation type="vanished">Copiar de la salida de octave</translation>
    </message>
    <message>
        <source>exec</source>
        <translation type="vanished">Ejecutar</translation>
    </message>
</context>
<context>
    <name>frameDlg</name>
    <message>
        <location filename="framedlg.cpp" line="504"/>
        <location filename="framedlg.cpp" line="507"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="framedlg.cpp" line="505"/>
        <source>% The source code written here will be executed when</source>
        <oldsource>% The source code writed here will be executed when</oldsource>
        <translation>% El código fuente escrito aquí será ejecutado cuando</translation>
    </message>
    <message>
        <location filename="framedlg.cpp" line="506"/>
        <source>% windows load. Work like &apos;onLoad&apos; event of other languages.</source>
        <translation>% la ventana se haga visible. Funciona como el evento &apos;onLoad&apos; de otros lenguajes.</translation>
    </message>
</context>
<context>
    <name>iconChooserWnd</name>
    <message>
        <location filename="iconchooserwnd.ui" line="14"/>
        <source>Icon selection</source>
        <translation>Selección de iconos</translation>
    </message>
    <message>
        <location filename="iconchooserwnd.ui" line="52"/>
        <source>Width:</source>
        <translation>Ancho:</translation>
    </message>
    <message>
        <location filename="iconchooserwnd.ui" line="59"/>
        <location filename="iconchooserwnd.ui" line="73"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="iconchooserwnd.ui" line="66"/>
        <source>Height:</source>
        <translation>Alto:</translation>
    </message>
    <message>
        <location filename="iconchooserwnd.ui" line="84"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="iconchooserwnd.ui" line="104"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="iconchooserwnd.cpp" line="22"/>
        <location filename="iconchooserwnd.cpp" line="41"/>
        <source>[None]</source>
        <translation>[Ninguno]</translation>
    </message>
</context>
<context>
    <name>mainWnd</name>
    <message>
        <location filename="mainwnd.ui" line="14"/>
        <source>guiEditor</source>
        <oldsource>GUI Editor</oldsource>
        <translation>guiEditor</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="57"/>
        <source>File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="87"/>
        <source>View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="96"/>
        <location filename="mainwnd.ui" line="434"/>
        <source>About</source>
        <translation>&amp;Sobre</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="116"/>
        <source>Control Toolbar</source>
        <translation>Barra de herramientas de controles</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="156"/>
        <source>File toolbar</source>
        <translation>Barra de herramientas de archivo</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="175"/>
        <source>Debug toolbar</source>
        <translation>Barra de herramientas de depuración</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="189"/>
        <source>Hardware Devices</source>
        <translation>Dispositivos de hardware</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">Nuevo...</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="219"/>
        <source>Button</source>
        <translation></translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Guardar</translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation type="vanished">Guardar como...</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="254"/>
        <location filename="mainwnd.cpp" line="99"/>
        <source>Properties</source>
        <translation>&amp;Propiedades</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="259"/>
        <source>Exit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="268"/>
        <source>Label</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="280"/>
        <source>Combo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="283"/>
        <source>Add combo box</source>
        <translation>Agregar un pop up menu</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="292"/>
        <source>Edit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="295"/>
        <source>Add a line edit</source>
        <translation>Agregar un cuadro de edición</translation>
    </message>
    <message>
        <source>Align palette</source>
        <translation type="vanished">Ventana de alineación</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="300"/>
        <source>Property Editor</source>
        <translation>Editor de propiedades</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="309"/>
        <source>Export dialog...</source>
        <translation>Exportar ventana...</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="314"/>
        <location filename="mainwnd.cpp" line="114"/>
        <source>Source code editor</source>
        <translation>Editor de código fuente</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="317"/>
        <source>Make the source code editor visible.</source>
        <translation>Hace visible el editor de código fuente.</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="326"/>
        <source>List</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="329"/>
        <source>Add a list control</source>
        <translation>Agregar un control list</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="338"/>
        <source>Check box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="341"/>
        <source>Add a checkbox control</source>
        <translation>Agregar un control check box</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="350"/>
        <source>Radio Button</source>
        <translation>Botón de radio</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="353"/>
        <source>Add a radio button control</source>
        <translation>Agregar un control de tipo botón de radio</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="406"/>
        <source>Run script</source>
        <translation>Ejecutar script</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="482"/>
        <source>GroupBox</source>
        <translation>GroupBox</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="485"/>
        <source>Add a group box panel</source>
        <translation>Agregar un control panel (uipanel)</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="490"/>
        <source>View project manager</source>
        <oldsource>View project mannager</oldsource>
        <translation>Administrador de proyectos</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="493"/>
        <source>View project manager windows</source>
        <translation>Ver la ventana de gestion de proyectos</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="502"/>
        <source>New Project</source>
        <translation>&amp;Nuevo proyecto</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="511"/>
        <source>Open Project</source>
        <translation>&amp;Abrir un proyecto</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="520"/>
        <source>Save Project</source>
        <translation>&amp;Guardar proyecto</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="529"/>
        <source>Run project</source>
        <translation>Ejecutar &amp;proyecto</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="534"/>
        <source>Close project</source>
        <translation>&amp;Cerrar un proyecto</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="537"/>
        <source>Close active project</source>
        <translation>Cerrar el proyecto activo</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="546"/>
        <source>CallBack</source>
        <translation>Callback</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="549"/>
        <source>Add a callback definition control</source>
        <translation>Agrega un control &apos;callback&apos;</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="558"/>
        <source>Add button group</source>
        <translation>Agrega un control button group (uibuttongroup)</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="561"/>
        <source>Add a new button group</source>
        <translation>Agrega un control button group (uibuttongroup)</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="566"/>
        <source>1 -</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="571"/>
        <source>2 -</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="576"/>
        <source>3 -</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="581"/>
        <source>4 -</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="586"/>
        <source>5 - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="595"/>
        <source>Table</source>
        <translation>Tabla</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="598"/>
        <source>Add a table control (uitable)</source>
        <translation>Agregar un control tabla (uitable)</translation>
    </message>
    <message>
        <source>Add a panel container</source>
        <translation type="vanished">Agregar un contenedor panel</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="370"/>
        <source>Toggle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="373"/>
        <source>Add a toggle button control</source>
        <translation>Agregar un control toggle</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="382"/>
        <source>Slider</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="385"/>
        <source>Add a slider control</source>
        <translation>Agregar un control slider</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="394"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="397"/>
        <source>Add a image control based on axes</source>
        <translation>Agregar un control axes</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="446"/>
        <source>Arduino Nano</source>
        <translation>Arduino Nano</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="449"/>
        <source>Add a Arduino Nano control</source>
        <translation>Agregar un control Arduino Nano</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="458"/>
        <source>Node MCU</source>
        <translation>Node MCU</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="461"/>
        <source>Add a Node MCU Control</source>
        <translation>Agregar un control Node MCU</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="470"/>
        <source>Edu CIAA</source>
        <translation>EDU CIAA</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="473"/>
        <source>Add a Edu CIAA board</source>
        <translation>Agregar un control EDU CIAA</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="102"/>
        <source>Run</source>
        <translation>&amp;Ejecutar</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="61"/>
        <source>Recent projects</source>
        <translation>Proyectos recientes</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="207"/>
        <source>New dialog</source>
        <translation>Nueva ventana de diálogo</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="210"/>
        <source>New stand alone dialog</source>
        <translation>Nueva ventana de diálogo independiente</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="228"/>
        <source>Open dialog</source>
        <translation>Abrir una ventana de diálogo</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="237"/>
        <source>Save dialog</source>
        <translation>Guardar una ventana de diálogo</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="246"/>
        <source>Save dialog as ....</source>
        <translation>Guardar una ventana como ...</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="249"/>
        <source>Save dialog as ...</source>
        <translation>Cuardar ventana como ...</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="271"/>
        <source>Add label</source>
        <translation>Agregar etiqueta</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="362"/>
        <source>Frame</source>
        <translation>Panel</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="365"/>
        <source>Add a frame control</source>
        <translation>Agregar un control Panel</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="409"/>
        <source>Test script in octave</source>
        <translation>Ejecutar el script en octave</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="418"/>
        <source>Stop</source>
        <translation>&amp;Detener</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="421"/>
        <source>Stop running script in octave</source>
        <translation>Detener la ejecución de octave</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="426"/>
        <source>Octave Console</source>
        <translation>Consola de GNU Octave</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="429"/>
        <source>View the Octave console</source>
        <translation>Ver la consola de GNU Octave</translation>
    </message>
    <message>
        <location filename="mainwnd.ui" line="437"/>
        <source>About GUIEditor</source>
        <translation>Sobre GUIEditor</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="134"/>
        <location filename="mainwnd.cpp" line="214"/>
        <location filename="mainwnd.cpp" line="618"/>
        <source>Starting octave...</source>
        <translation>Iniciando GNU Octave...</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="158"/>
        <location filename="mainwnd.cpp" line="239"/>
        <location filename="mainwnd.cpp" line="641"/>
        <source>started.</source>
        <translation>iniciado.</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="763"/>
        <source>Open gui project</source>
        <translation>Abrir un proyecto de interfáz gráfica</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="763"/>
        <source>GUI Project files (*.prj);;All files (*.*);;</source>
        <translation>Archivos de proyectos de interfáz gráfica (*.prj);; Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="824"/>
        <location filename="mainwnd.cpp" line="843"/>
        <location filename="mainwnd.cpp" line="862"/>
        <location filename="mainwnd.cpp" line="881"/>
        <location filename="mainwnd.cpp" line="900"/>
        <source>The project is already open</source>
        <translation>El proyecto ya se encuentra abierto</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="824"/>
        <location filename="mainwnd.cpp" line="843"/>
        <location filename="mainwnd.cpp" line="862"/>
        <location filename="mainwnd.cpp" line="881"/>
        <location filename="mainwnd.cpp" line="900"/>
        <source>The project </source>
        <translation>El proyecto</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="824"/>
        <location filename="mainwnd.cpp" line="843"/>
        <location filename="mainwnd.cpp" line="862"/>
        <location filename="mainwnd.cpp" line="881"/>
        <location filename="mainwnd.cpp" line="900"/>
        <source> is open in the editor. A project cannot be open more than once in the editor.</source>
        <translation>ya está abierto. Cada proyecto puede abrirse una vez dentro del editor.</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="834"/>
        <location filename="mainwnd.cpp" line="853"/>
        <location filename="mainwnd.cpp" line="872"/>
        <location filename="mainwnd.cpp" line="891"/>
        <location filename="mainwnd.cpp" line="910"/>
        <source>Project not found</source>
        <translation>El proyecto no se ha encontrado</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="834"/>
        <location filename="mainwnd.cpp" line="853"/>
        <location filename="mainwnd.cpp" line="872"/>
        <location filename="mainwnd.cpp" line="891"/>
        <location filename="mainwnd.cpp" line="910"/>
        <source>The project &quot;</source>
        <translation>El proyecto &quot;</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="834"/>
        <location filename="mainwnd.cpp" line="853"/>
        <location filename="mainwnd.cpp" line="872"/>
        <location filename="mainwnd.cpp" line="891"/>
        <location filename="mainwnd.cpp" line="910"/>
        <source>&quot; does not exist!</source>
        <translation>&quot; no existe!</translation>
    </message>
    <message>
        <source>Select destination file name</source>
        <translation type="vanished">Seleccione el archivo de destino</translation>
    </message>
    <message>
        <source>XML Dialogs (*.dlg);;All files (*.*);;</source>
        <translation type="vanished">Diálogos XML(*.xml);;Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="442"/>
        <source>XML Dialogs (*.xml);;All files (*);;</source>
        <translation>Diálodos XML (*.xml);;Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="442"/>
        <source>Select dialog to open</source>
        <translation>Seleccione el diálogo que desea abrir</translation>
    </message>
    <message>
        <source>XML Dialogs (*.dlg);;All files (*);;</source>
        <translation type="vanished">Diálogos XML(*.dlg);;Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="463"/>
        <source>Target folder</source>
        <translation>Carpeta de destino</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="251"/>
        <location filename="mainwnd.cpp" line="652"/>
        <source>Bad configuration</source>
        <translation>Configuración equivocada</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="93"/>
        <source>Project Manager</source>
        <oldsource>Project Mannager</oldsource>
        <translation>Gestor de proyectos</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="107"/>
        <source>Octave console</source>
        <translation>Consola de GNU Octave</translation>
    </message>
    <message>
        <location filename="mainwnd.cpp" line="251"/>
        <location filename="mainwnd.cpp" line="652"/>
        <source>To run this script is neccesary configure the temporary directory and the octave path from the File/Property menu</source>
        <translation>Para ejecutar este script es necesario que configure la ruta a una carpeta temporal y la carpeta donde se encuentra instalado octave desde Archivo/Propiedades</translation>
    </message>
</context>
<context>
    <name>mfileEditorWidget</name>
    <message>
        <location filename="mfileeditorwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Ventana</translation>
    </message>
</context>
<context>
    <name>newFileWnd</name>
    <message>
        <location filename="newfilewnd.ui" line="14"/>
        <source>Add new file</source>
        <translation>Agregar un nuevo archivo</translation>
    </message>
    <message>
        <location filename="newfilewnd.ui" line="22"/>
        <source>File name:</source>
        <translation>Nombre del archivo:</translation>
    </message>
    <message>
        <location filename="newfilewnd.ui" line="49"/>
        <source>A file with this name alredy exists.</source>
        <translation>Un archivo con el mismo nombre ya existe</translation>
    </message>
    <message>
        <location filename="newfilewnd.ui" line="73"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="newfilewnd.ui" line="93"/>
        <source>Add</source>
        <translation>Agregar</translation>
    </message>
</context>
<context>
    <name>newProjectWnd</name>
    <message>
        <location filename="newprojectwnd.ui" line="14"/>
        <source>Project setup</source>
        <translation>Configuración del proyecto</translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="22"/>
        <source>Project name:</source>
        <translation>Nombre del proyecto:</translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="36"/>
        <source>Project path:</source>
        <translation>Ubicación del proyecto:</translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="46"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="57"/>
        <source>Version:</source>
        <translation>Versión: </translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="131"/>
        <source>The tarjet folder with this name alredy exists.</source>
        <translation>Ya existe una carpeta con el nombre indicado.</translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="155"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="newprojectwnd.ui" line="178"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="newprojectwnd.cpp" line="41"/>
        <source>Project folder</source>
        <translation>Carpeta del proyecto</translation>
    </message>
</context>
<context>
    <name>pkgGenWnd</name>
    <message>
        <location filename="pkggenwnd.ui" line="14"/>
        <source>Generate octave package</source>
        <translation>Generar paquete de Octave</translation>
    </message>
    <message>
        <location filename="pkggenwnd.ui" line="25"/>
        <source>Open Folder</source>
        <translation>Abrir carpeta</translation>
    </message>
    <message>
        <location filename="pkggenwnd.ui" line="45"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="52"/>
        <location filename="pkggenwnd.cpp" line="87"/>
        <source>Deleting previus version of </source>
        <translation>Borrando la versión anterior de</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="62"/>
        <source>Staring tar...</source>
        <translation>Iniciando tar ...</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="75"/>
        <source> [started]</source>
        <translation> [iniciado]</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="83"/>
        <source>Tar finished Ok</source>
        <translation>Tar finalizó correctamente</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="93"/>
        <source>Staring gzip...</source>
        <translation>Iniciando gzip ...</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="97"/>
        <source>Tar call fail. </source>
        <translation>Falló la invocación a Tar. </translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="123"/>
        <source>Generation finished!</source>
        <translation>Generación finalizada!</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="124"/>
        <source>You can open the container folder to use the package</source>
        <translation>Puede abrir la carpeta que contiene el paquete a fin de utilizarlo</translation>
    </message>
    <message>
        <location filename="pkggenwnd.cpp" line="127"/>
        <source>Error gziping package.</source>
        <translation>Error comprimiendo el paquete.</translation>
    </message>
</context>
<context>
    <name>prjPropWnd</name>
    <message>
        <location filename="prjpropwnd.ui" line="14"/>
        <source>Project Properties</source>
        <translation>Propiedades del proyecto</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="20"/>
        <source>Project version:</source>
        <translation>Versión del proyecto:</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="38"/>
        <source>Define _basePath var</source>
        <translation>Definir la variable _basePath</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="45"/>
        <source>Define _imgPath var</source>
        <translation>Definir la variable _imgPath</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="67"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="prjpropwnd.ui" line="87"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
</context>
<context>
    <name>projectWnd</name>
    <message>
        <source>Gestor de proyectos</source>
        <translation type="vanished">Gestor de proyectos</translation>
    </message>
    <message>
        <source>Proyectos Abiertos</source>
        <translation type="vanished">Proyectos abiertos</translation>
    </message>
    <message>
        <source>Se perderá información</source>
        <translation type="vanished">Se perderá información</translation>
    </message>
    <message>
        <source>Algunos archivos que forman parte del proyecto no han sido guardados. Desea guardar los cambios?</source>
        <translation type="vanished">Algunos archivos que forman parte del proyecto no han sido guardados. Desea guardar los cambios?</translation>
    </message>
</context>
<context>
    <name>propertiesWndClass</name>
    <message>
        <source>Propperty editor</source>
        <translation type="vanished">Editor de propiedades</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nombre</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Valor</translation>
    </message>
</context>
<context>
    <name>propertyWndClass</name>
    <message>
        <location filename="propertywndclass.ui" line="14"/>
        <source>GUIEditor Properties</source>
        <translation>Configuración de guiEditor</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="403"/>
        <source>Debug options</source>
        <translation>Opciones de depuración</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="409"/>
        <source>Path to octave app:</source>
        <translation>Directorio de octave:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="83"/>
        <location filename="propertywndclass.ui" line="114"/>
        <location filename="propertywndclass.ui" line="157"/>
        <location filename="propertywndclass.ui" line="191"/>
        <location filename="propertywndclass.ui" line="246"/>
        <location filename="propertywndclass.ui" line="418"/>
        <location filename="propertywndclass.ui" line="507"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="68"/>
        <source>Package generation tools</source>
        <translation>Herramientas para la generación de paquetes</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="74"/>
        <source>gzip path:</source>
        <translation>ruta a gzip:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="96"/>
        <location filename="propertywndclass.ui" line="127"/>
        <location filename="propertywndclass.ui" line="170"/>
        <location filename="propertywndclass.ui" line="204"/>
        <location filename="propertywndclass.ui" line="259"/>
        <location filename="propertywndclass.ui" line="437"/>
        <location filename="propertywndclass.ui" line="489"/>
        <location filename="propertywndclass.ui" line="526"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="105"/>
        <source>tar path:</source>
        <translation>ruta a tar:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="139"/>
        <source>Fonts types</source>
        <translation>Tipos de fuentes</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="147"/>
        <source>GUI Controls:</source>
        <translation>Controls gráficos:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="181"/>
        <source>Source code editor:</source>
        <translation>Editor de código fuente:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="218"/>
        <source>Source code editor tab size:</source>
        <translation>Ancho de tabulación en el editor de código fuente:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="239"/>
        <source>Default work dir:</source>
        <translation>Directorio de trabajo por defecto:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="282"/>
        <source>Output encoding</source>
        <translation>Codificación de salida</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="288"/>
        <source>This option sets the output codec for script files. The typical value is UTF-8, but you can configure other values from the list if you want.</source>
        <translation>Esta opción establece el códec de salida para los archivos de script. El valor típico es UTF-8, pero puede configurar otros valores de la lista si lo desea.</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="313"/>
        <source>Output codec:</source>
        <translation>Codec a utilizar:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="342"/>
        <source>If you set the following option, the codec indicated in the GNU Octave __mfile_encoding__ variable will be set prior to the main window&apos;s construction code. After the execution of the application is finished, the previous value will be restored.</source>
        <translation>Si establece la siguiente opción, se modificará el valor de la variable __mfile_encoding__ de GNU Octave al valor configurado como codec de salida. Este cambio se realizará antes ejecutar el código de creación de la ventana principal. Una vez finalizada la ejecución de la aplicación, se intentará restaurar el valor original.</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="367"/>
        <source>Force codec settings</source>
        <translation>Forzar la configuación del codec</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="446"/>
        <source>QT_PLUGIN_PATH:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="470"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="498"/>
        <source>Temp dir path:</source>
        <translation>Directorio temporal:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="537"/>
        <source>Librarys path:</source>
        <translation>Ruta a librerías que incluir:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="569"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="588"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="24"/>
        <source>General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="32"/>
        <source>Language:</source>
        <translation>Idioma:</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="40"/>
        <source>Spanish</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="45"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="619"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="propertywndclass.ui" line="639"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="38"/>
        <source>Octave file path</source>
        <translation>Ruta al binario de octave</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="38"/>
        <source>Octave binary(octave*);;All files (*.*);;</source>
        <translation>Binario de octave (octave*);;Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="50"/>
        <source>Termporal dir path</source>
        <translation>Directorio temporal</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="61"/>
        <source>Library path to add</source>
        <translation>Ruta a la librería</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="77"/>
        <source>QT_PLUGIN_PATH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="92"/>
        <source>gzip file path</source>
        <translation>Ruta al archivo gzip</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="92"/>
        <source>gzip binary(gzip*);;All files (*.*);;</source>
        <translation>Binario de gzip (gzip*);; Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="102"/>
        <source>tar file path</source>
        <translation>Ruta al archivo tar</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="102"/>
        <source>tar binary(tar*);;All files (*.*);;</source>
        <translation>Binario de tar (tar*);;Todos los archivos (*.*);;</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="113"/>
        <location filename="propertywndclass.cpp" line="124"/>
        <source>Select controls font name</source>
        <translation>Seleccione la fuente para los controles</translation>
    </message>
    <message>
        <location filename="propertywndclass.cpp" line="136"/>
        <source>Default work directory</source>
        <translation>Seleccionar directorio de trabajo por defecto</translation>
    </message>
</context>
<context>
    <name>runArgsWnd</name>
    <message>
        <location filename="runargswnd.ui" line="14"/>
        <source>Setup run arguments</source>
        <translation>Configuración de los argumentos de ejecución</translation>
    </message>
    <message>
        <location filename="runargswnd.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The indicated values will be used as application arguments and sent to the main window as varargin. If you want to use more than one argument, you must separate them by commas (for example 7,8,9). If it is a string, you must include quotes (for example &amp;quot;hello&amp;quot;, &amp;quot;World&amp;quot;).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Los valores indicados se usarán como argumentos de la aplicación y se enviarán a la ventana principal como varargin. Si desea utilizar más de un argumento, debe separarlos con comas (por ejemplo, 7,8,9). Si es una cadena, debe incluir comillas (por ejemplo &amp;quot;hola&amp;quot;, &amp;quot;Mundo&amp;quot;).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="runargswnd.ui" line="32"/>
        <source>Arguments to use:</source>
        <translation>Valor de los argumentos:</translation>
    </message>
    <message>
        <location filename="runargswnd.ui" line="59"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="runargswnd.ui" line="79"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
</context>
<context>
    <name>sourceWnd</name>
    <message>
        <source>Source code callback</source>
        <translation type="vanished">Código fuente de función de callback</translation>
    </message>
    <message>
        <source>Associated control:</source>
        <translation type="vanished">Control asociado:</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Guardar</translation>
    </message>
    <message>
        <source>Load</source>
        <translation type="vanished">Cargar</translation>
    </message>
    <message>
        <source>Save this source code as...</source>
        <translation type="vanished">Guardar este código fuente como...</translation>
    </message>
    <message>
        <source>Load source code as callback...</source>
        <translation type="vanished">Cargar código fuente como función de callback...</translation>
    </message>
</context>
<context>
    <name>treeItemDialog</name>
    <message>
        <location filename="childwnddlg.cpp" line="60"/>
        <source>Set as main dialog</source>
        <translation>Establecer como ventana inicial</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="61"/>
        <source>Duplicate</source>
        <translation>Duplicar</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="62"/>
        <source>Rename</source>
        <translation>Renombrar</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="63"/>
        <source>Delete </source>
        <translation>Borrar </translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="68"/>
        <source>Name for dialog copy</source>
        <translation>Nombre para la copia</translation>
    </message>
    <message>
        <location filename="childwnddlg.cpp" line="96"/>
        <source>New name for dialog</source>
        <translation>Nuevo nombre para la ventana de diálogo</translation>
    </message>
    <message>
        <source>Not implemente</source>
        <translation type="vanished">No implementado</translation>
    </message>
    <message>
        <source>Ups.. we are working on it!</source>
        <translation type="vanished">Ups... Estamos trabajando en esto! Disculpe los inconvenientes.</translation>
    </message>
</context>
<context>
    <name>treeItemImage</name>
    <message>
        <location filename="childwndimg.cpp" line="45"/>
        <source>Delete </source>
        <translation>Borrar </translation>
    </message>
</context>
<context>
    <name>treeItemMFile</name>
    <message>
        <location filename="childwndmfile.cpp" line="57"/>
        <source>Delete</source>
        <oldsource>Delete </oldsource>
        <translation>Borrar</translation>
    </message>
    <message>
        <source>Not implemente</source>
        <translation type="vanished">No implementado</translation>
    </message>
    <message>
        <source>Ups.. we are working on it!</source>
        <translation type="vanished">Ups... estamos trabajadno en esto! Disculpe las molestias.</translation>
    </message>
</context>
<context>
    <name>treeItemProject</name>
    <message>
        <location filename="guiproject.cpp" line="75"/>
        <source>Add a new dialog</source>
        <translation>Agregar una nueva ventana de diálgo</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="76"/>
        <source>Add a new m file</source>
        <translation>Agregar un numero archivo script (.m)</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="85"/>
        <source>Run project</source>
        <translation>Ejecutar el proyecto</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="79"/>
        <source>Export project</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="77"/>
        <source>Add a new image</source>
        <translation>Agregar una imagen nueva</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="80"/>
        <source>Generate octave package</source>
        <translation>Generar paquete</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="82"/>
        <source>Save project</source>
        <translation>Guardar proyecto</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="83"/>
        <source>Close project</source>
        <translation>Cerrar proyecto</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="86"/>
        <source>Set run arguments</source>
        <translation>Establecer el valor de los argumentos</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="88"/>
        <source>Properties</source>
        <translation>Propiedades</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="163"/>
        <source>Select image to add</source>
        <translation>Seleccione la imagen a agregar</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="165"/>
        <source>PNG Files (*.png);;SVG Files (*.svg);;All files (*.*);;</source>
        <translation>Archivos png (*.png);; Archivos svg (*.svg);; Todos los archivos(*.*);;</translation>
    </message>
    <message>
        <location filename="guiproject.cpp" line="215"/>
        <location filename="guiproject.cpp" line="218"/>
        <source>Define </source>
        <translation>Definir </translation>
    </message>
    <message>
        <source>Not implemente</source>
        <translation type="vanished">No implementado</translation>
    </message>
    <message>
        <source>Ups.. we are working on it!</source>
        <translation type="vanished">Ups... estamos trabajando en esto! Disculpe por favor!</translation>
    </message>
</context>
<context>
    <name>treeItemTextFile</name>
    <message>
        <location filename="childwndtextfile.cpp" line="44"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
</context>
<context>
    <name>wdgArgEditor</name>
    <message>
        <location filename="wdgargeditor.ui" line="14"/>
        <source>Form</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="wdgargeditor.ui" line="50"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="wdgargeditor.cpp" line="67"/>
        <source>Callback argument editor</source>
        <translation>Editor de argumentos de callback</translation>
    </message>
</context>
<context>
    <name>wdgClrChooser</name>
    <message>
        <location filename="wdgclrchooser.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wdgclrchooser.ui" line="38"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>wdgDebug</name>
    <message>
        <location filename="wdgdebug.ui" line="14"/>
        <source>Form</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="wdgdebug.ui" line="47"/>
        <source>Clear output console</source>
        <translation>Borrar la salida de consola</translation>
    </message>
    <message>
        <location filename="wdgdebug.ui" line="61"/>
        <source>Copy selected lines from output console</source>
        <translation>Copiar de la salida de octave</translation>
    </message>
    <message>
        <location filename="wdgdebug.ui" line="96"/>
        <source>exec</source>
        <translation>Ejecutar</translation>
    </message>
    <message>
        <location filename="wdgdebug.cpp" line="158"/>
        <source>Octave is not running!</source>
        <translation>Octave no se está ejecutando</translation>
    </message>
</context>
<context>
    <name>wdgIconChooser</name>
    <message>
        <location filename="wdgiconchooser.ui" line="14"/>
        <source>Form</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="wdgiconchooser.ui" line="57"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wdgiconchooser.ui" line="76"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="wdgiconchooser.cpp" line="56"/>
        <source>[None]</source>
        <translation>[Ninguno]</translation>
    </message>
</context>
<context>
    <name>wdgItemEditor</name>
    <message>
        <location filename="wdgitemeditor.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wdgitemeditor.ui" line="50"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>wdgPrjMan</name>
    <message>
        <location filename="wdgprjman.ui" line="14"/>
        <source>Form</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="wdgprjman.ui" line="36"/>
        <source>Open projects</source>
        <translation>Proyectos abiertos</translation>
    </message>
    <message>
        <location filename="wdgprjman.cpp" line="117"/>
        <source>Se perderá información</source>
        <translation>Se perderá información</translation>
    </message>
    <message>
        <location filename="wdgprjman.cpp" line="118"/>
        <source>Algunos archivos que forman parte del proyecto no han sido guardados. Desea guardar los cambios?</source>
        <translation>Algunos archivos que forman parte del proyecto no han sido guardados. Desea guardar los cambios?</translation>
    </message>
</context>
<context>
    <name>wdgProp</name>
    <message>
        <location filename="wdgprop.ui" line="14"/>
        <source>Form</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="wdgprop.ui" line="48"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="wdgprop.ui" line="63"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
</context>
<context>
    <name>wdgSrcEditor</name>
    <message>
        <location filename="wdgsrceditor.ui" line="14"/>
        <source>Form</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="wdgsrceditor.ui" line="55"/>
        <source>Associated control:</source>
        <translation>Control asociado:</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Guardar</translation>
    </message>
    <message>
        <source>Load</source>
        <translation type="vanished">Cargar</translation>
    </message>
</context>
<context>
    <name>wndItemEdit</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Editar opciones</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="14"/>
        <source>List item editor</source>
        <translation>Editor de items</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="22"/>
        <source>New val:</source>
        <translation>Nuevo valor:</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="32"/>
        <source>Add</source>
        <translation>Agregar</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="39"/>
        <source>Delete selected</source>
        <translation>Borrar seleccionado</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="42"/>
        <source>Del</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="58"/>
        <source>Up</source>
        <translation>Subir</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="78"/>
        <source>Down</source>
        <translation>Bajar</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="91"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="wnditemedit.ui" line="111"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
</TS>
