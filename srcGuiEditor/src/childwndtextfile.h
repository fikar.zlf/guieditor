/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef CHILDWNDTEXTFILE_H
#define CHILDWNDTEXTFILE_H

#include "abstractchildwnd.h"
#include "projectitem.h"
#include "guiproject.h"
#include <Qsci/qsciscintilla.h>

class childWndTextFile;

class treeItemTextFile :  public abstractPrjTreeItem
{
    Q_OBJECT
private:
    childWndTextFile * dlg;
    guiProject * associatedPrj;
public:
    explicit treeItemTextFile(guiProject * p, childWndTextFile *dlg,  QTreeWidgetItem * parent = 0);
    void activate();
    void populateMenu(QMenu &);
    void selectProject();
private slots:
    void clicked();
    void actDelete(bool checked = false);
};

class childWndTextFile : public abstractChildWnd, public projectItem
{
    Q_OBJECT
private:
    QsciScintilla * editor;
public:
    childWndTextFile(QWidget *parent, Qt::WindowFlags flags, guiProject *prj);
    void save();
    void save(QString);
    void saveAs();
    void load(QString fn);
    void run(QString path);
    void exportAsScript(QString);
    void activate(int line = -1, int col = -1);
    void closingProject();
    QString getName();
    virtual bool isModified();
    abstractPrjTreeItem * treeItemWidget(abstractPrjTreeItem *);
};

#endif // CHILDWNDTEXTFILE_H
