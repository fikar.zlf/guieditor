/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef IMGCTRL_H
#define IMGCTRL_H
#include <QWidget>
#include "abstractuicctrl.h"
#include <QLabel>
#include <QFontMetrics>
#include "commonproperties.h"
#include "controlgenerator.h"


class imgCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;

public:
    static unsigned int getNameCounter();
    imgCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    virtual QString className() { return "imgCtrl";}
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool);
    virtual ~imgCtrl();
};

class imgCtrlGen: public controlGenerator
{
public:
  imgCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // IMGCTRL_H


