/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "childwndimg.h"
#include <QDir>
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QTextStream>
#include <QMessageBox>

treeItemImage::treeItemImage(guiProject * p, childWndImg *dlg,  QTreeWidgetItem * parent):
    abstractPrjTreeItem(parent)
{
    this->setText(0, dlg->windowTitle());
    this->setToolTip(0, dlg->documentName);
    this->setIcon(0, QPixmap(":/img/imgItem"));
    this->dlg = dlg;
    this->associatedPrj = p;
}

void treeItemImage::activate()
{

    dlg->show();
    dlg->widget()->show();
}

void treeItemImage::populateMenu(QMenu &m)
{
    m.addAction(tr("Delete ") + dlg->windowTitle(), this, SLOT(actDelete(bool)));
}

void treeItemImage::selectProject()
{
    mainWnd::getPrjWnd()->setActiveProject(this->associatedPrj);
}

void treeItemImage::clicked()
{
    this->activate();
}

void treeItemImage::actDelete(bool checked __attribute__ ((unused)))
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(mainWnd::getMainWnd(),
                                  QObject::tr("Warning!!! This operation cannot be undone"),
                                  QObject::tr("You are trying to delete the file %1, this operation can't be undone. Are you sure to delete this file?").arg(this->dlg->documentName),
                                  QMessageBox::Yes | QMessageBox::No);
    if(reply == QMessageBox::Yes)
    {
        QFile toDelete(this->dlg->documentName);
        toDelete.remove();
        QFileInfo fi(this->dlg->documentName);

        QFile runCopy(this->associatedPrj->runPath() + QDir::separator() + "img" + QDir::separator() + fi.fileName());
        if(runCopy.exists())
            runCopy.remove();

        QFile exportCopy(this->associatedPrj->exportPath() + QDir::separator() + "img" + QDir::separator() + fi.fileName());
        if(exportCopy.exists())
            exportCopy.remove();

        this->dlg->close();
        this->associatedPrj->items.removeAll(this->dlg);
        mainWnd::getMainWnd()->ui->mdiArea->removeSubWindow(this->dlg);        
        this->dlg->deleteLater();
        mainWnd::getPrjWnd()->refreshList();
    }
}

void childWndImg::resizeEvent(QResizeEvent * resizeEvent)
{
    resizeEvent->accept();
    this->labImg->adjustSize();
    this->labImg->setPixmap(QPixmap(documentName).scaled(this->labImg->size(),Qt::KeepAspectRatio));
    labImg->setScaledContents(true);
    abstractChildWnd::resizeEvent(resizeEvent);
}

childWndImg::childWndImg(QWidget *parent, Qt::WindowFlags flags, guiProject * prj)
  :abstractChildWnd(parent, flags),projectItem("Image", prj)
{

    labImg = new QLabel(this);
    labImg->setScaledContents(true);
    this->setGeometry(0, 0, 320, 240);
    this->setWidget(labImg);
    labImg->setPixmap(QPixmap(":/img/imgSymbol").scaled(this->width(), this->height(),Qt::KeepAspectRatio));
}

void childWndImg::save()
{
    /* This method is not implemented because any change can
     * be made over images... in this version ;)
     */
}

void childWndImg::save(QString)
{
    /* This method is not implemented because any change can
     * be made over images... in this version ;)
     */
}

void childWndImg::saveAs()
{
    /* This method is not implemented because any change can
     * be made over images... in this version ;)
     */
}

void childWndImg::load(QString fn)
{    
    QPixmap img(fn);
    this->documentHaveName = true;
    this->documentName = fn;
    this->labImg->setPixmap(img.scaled(this->width(), this->height(),Qt::KeepAspectRatio));
}

void childWndImg::run(QString path)
{
    QFile f;
    this->exportAsScript(path);
    f.setFileName(path + "/runApp.m");
    f.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream txt(&f);
    txt.setCodec(mainWnd::getAppConfig()->getOutputCodec().toStdString().c_str());
    txt << "function ret = runApp()" << endl;
    txt << "  [dir, name, ext] = fileparts( mfilename('fullpathext') );" << endl;
    txt << "  ret = imshow('" << this->documentName << "');" << endl;
    txt << "  refresh();" << endl;
    txt << "end" << endl;
    f.close();
}

void childWndImg::exportAsScript(QString path)
{
    QDir outPath(path);
    QFileInfo imgInfo(this->documentName);
    if(!outPath.exists("img"))
        outPath.mkdir("img");
    outPath.cd("img");
    QFile::copy(this->documentName, outPath.path() + QDir::separator() + imgInfo.fileName());
}

void childWndImg::activate(int line __attribute__((unused)), int col __attribute__((unused)))
{
    this->show();
    this->labImg->show();
}

void childWndImg::closingProject()
{
    close();
    mainWnd::getMainWnd()->ui->mdiArea->removeSubWindow(this);
}

QString childWndImg::getName()
{
    QFileInfo fileInfo(documentName);
    return fileInfo.fileName();
}

bool childWndImg::isModified()
{
  return false;
}

abstractPrjTreeItem * childWndImg::treeItemWidget(abstractPrjTreeItem *treeItem)
{
  return new treeItemImage(dynamic_cast<treeItemProject*>(treeItem)->getPrj(), this, treeItem);
}
