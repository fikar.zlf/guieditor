/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef WDGSRCEDITOR_H
#define WDGSRCEDITOR_H

#include <QWidget>
#include <QMdiArea>
#include "abstractuicctrl.h"
#include "Qsci/qscilexermatlab.h"
namespace Ui {
class wdgSrcEditor;
}
class abstractUICCtrl;

class wdgSrcEditor : public QWidget
{
    Q_OBJECT
protected:
    void updateWidgetListRecursive(QList<abstractUICCtrl *> widgets);
public:
    explicit wdgSrcEditor(QWidget *parent, QMdiArea *mdi);
    ~wdgSrcEditor();
    void updateWidgetList(QList<abstractUICCtrl *> widgets, bool force = false);
    void closeChildWnd(void);
    void deleteControls(void);
    void actualWidget(abstractUICCtrl * ctrl);
    void updateWidgetsNames();
    void saveSourceCode();
    void closeDlg();
    void setCursorLine(int);
private slots:
    void changeCmbIndex(const QString &arg1);
private:
    Ui::wdgSrcEditor *ui;
    QMdiArea * clients;
    QList<abstractUICCtrl *> widgets;
    abstractUICCtrl * selectedWdg;
    QsciLexerMatlab * lex;
};

#endif // WDGSRCEDITOR_H
