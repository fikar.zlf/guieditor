/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "wdgprop.h"
#include "ui_wdgprop.h"
#include "abstractuicctrl.h"

wdgProp::wdgProp(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wdgProp)
{
    ui->setupUi(this);

}

QTableWidget * wdgProp::getTblProp(void)
{
    return ui->propTbl;
}

void wdgProp::updatePropertiesValues(void)
{
    int i;
    updPropWdg * w;
    for(i=0; i < ui->propTbl->rowCount(); i++)
    {
        w = dynamic_cast<updPropWdg *>(ui->propTbl->cellWidget(i, 1));
        w->updPropValue();
    }
}

wdgProp::~wdgProp()
{
    delete ui;
}
