/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef WDGCLRCHOOSER_H
#define WDGCLRCHOOSER_H

#include <QWidget>
#include <QLineEdit>
#include "abstractuicctrl.h"

namespace Ui {
class wdgClrChooser;
}

class wdgClrChooser : public QWidget, public updPropWdg
{
    Q_OBJECT
    
public:
    explicit wdgClrChooser(abstractPropEditor * prop);
    ~wdgClrChooser();
    void setColor(QString v);
    QString getColor();
    QLineEdit *getLEColor();
    virtual void updPropValue();
private slots:
    void on_btnColorChooser_clicked();

private:
    Ui::wdgClrChooser *ui;
};

#endif // WDGCLRCHOOSER_H
