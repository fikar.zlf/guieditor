/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "pkggenwnd.h"
#include "ui_pkggenwnd.h"
#include <QDir>
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QDesktopServices>

pkgGenWnd::pkgGenWnd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::pkgGenWnd)
{
    ui->setupUi(this);
    gzip = new QProcess(this);
    tar = new QProcess(this);
    connect(tar, SIGNAL(started()), this, SLOT(tarStarted()));
    connect(tar, SIGNAL(readyReadStandardOutput()), this, SLOT(tarReadyReadOutput()));
    connect(tar, SIGNAL(readyReadStandardError()), this, SLOT(tarReadyReadError()));
    connect(tar, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(tarFinished(int,QProcess::ExitStatus)));

    connect(gzip, SIGNAL(started()), this, SLOT(gzipStarted()));
    connect(gzip, SIGNAL(readyReadStandardOutput()), this, SLOT(gzipReadyReadOutput()));
    connect(gzip, SIGNAL(readyReadStandardError()), this, SLOT(gzipReadyReadError()));
    connect(gzip, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(gzipFinished(int,QProcess::ExitStatus)));

}

void pkgGenWnd::startGen(QString path, QString pkgName)
{
    QStringList arg;
    ui->lstCmd->clear();
    QFile targetFile(path + QDir::separator() + this->pkgName + ".tar");
    if(targetFile.exists())
    {
        ui->lstCmd->addItem(tr("Deleting previus version of ") + path + QDir::separator() + this->pkgName + ".tar");
        targetFile.remove();
    }
    this->path = path;
    this->pkgName = pkgName;
    show();

    arg << "-cf" << this->pkgName + ".tar" << this->pkgName;
    tar->setWorkingDirectory(path);
    tar->start(mainWnd::getMainWnd()->getAppConfig()->getTarPath(), arg);
    ui->lstCmd->addItem(tr("Staring tar..."));
    ui->btnClose->setEnabled(false);
}

pkgGenWnd::~pkgGenWnd()
{
    delete ui;
    delete gzip;
    delete tar;
}

void pkgGenWnd::tarStarted()
{
    ui->lstCmd->addItem(mainWnd::getMainWnd()->getAppConfig()->getTarPath() + tr(" [started]"));
}

void pkgGenWnd::tarFinished(int exitCode __attribute__((unused)), QProcess::ExitStatus exitStatus)
{
    QStringList arg;
    if(exitStatus == QProcess::NormalExit)
    {
        ui->lstCmd->addItem(tr("Tar finished Ok"));
        QFile targetFile(path + QDir::separator() + this->pkgName + ".tar.gz");
        if(targetFile.exists())
        {
            ui->lstCmd->addItem(tr("Deleting previus version of ") + path + QDir::separator() + this->pkgName + ".tar.gz");
            targetFile.remove();
        }
        arg << this->pkgName + ".tar";
        gzip->setWorkingDirectory(path);
        gzip->start(mainWnd::getMainWnd()->getAppConfig()->getGzipPath(), arg);
        ui->lstCmd->addItem(tr("Staring gzip..."));
    }
    else
    {
        ui->lstCmd->addItem(tr("Tar call fail. "));
        ui->btnClose->setEnabled(true);
    }
}

void pkgGenWnd::tarReadyReadOutput()
{
    QString output(tar->readAllStandardOutput());
    ui->lstCmd->addItems(output.split("\n"));
}

void pkgGenWnd::tarReadyReadError()
{
    QString output(tar->readAllStandardError());
    ui->lstCmd->addItems(output.split("\n"));
}

void pkgGenWnd::gzipStarted()
{
    ui->lstCmd->addItem("gzip started");
}

void pkgGenWnd::gzipFinished(int exitCode __attribute__ ((unused)), QProcess::ExitStatus exitStatus)
{
    if(exitStatus == QProcess::NormalExit)
    {
        ui->lstCmd->addItem(tr("Generation finished!"));
        ui->lstCmd->addItem(tr("You can open the container folder to use the package"));
    }
    else
        ui->lstCmd->addItem(tr("Error gziping package."));
    ui->btnClose->setEnabled(true);
}

void pkgGenWnd::gzipReadyReadOutput()
{
    QString output(gzip->readAllStandardOutput());
    ui->lstCmd->addItems(output.split("\n"));
}

void pkgGenWnd::gzipReadyReadError()
{
    QString output(gzip->readAllStandardError());
    ui->lstCmd->addItems(output.split("\n"));
}

void pkgGenWnd::on_btnOpen_clicked()
{
    QDesktopServices::openUrl(this->path);
}

void pkgGenWnd::on_btnClose_clicked()
{
    close();
}
