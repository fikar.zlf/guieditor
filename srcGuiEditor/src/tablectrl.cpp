/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "tablectrl.h"
#include "editabletable.h"
#include "storagemanager.h"

#include <QFile>
#include <QTextStream>
#include <QDir>
#include "wdgclrchooser.h"

tableBgClrPropEditor::tableBgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "BackgroundColor";
    palette = ctrl->associatedWidget()->palette();
    ctrl->associatedWidget()->setAutoFillBackground(true);
    ctrl->associatedWidget()->setPalette(palette);
}

QString tableBgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::Window).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::Window).blueF(), 'f', 3) + "]";
    return ret;
}

bool tableBgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void tableBgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ");
        palette.setColor(QPalette::Window, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString tableBgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'BackgroundColor', " + this->getValue();
    return ret;
}

QWidget * tableBgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void tableBgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}

tableFgClrPropEditor::tableFgClrPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
    this->propName = "ForegroundColor";
}

QString tableFgClrPropEditor::getValue()
{
    QString ret;
    palette = ctrl->associatedWidget()->palette();

    ret = "[";
    ret += QString::number(palette.color(QPalette::WindowText).redF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::WindowText).greenF(), 'f', 3) + " ";
    ret += QString::number(palette.color(QPalette::WindowText).blueF(), 'f', 3) + "]";
    return ret;
}

bool tableFgClrPropEditor::isValidValue(QString newVal __attribute__((unused)))
{
    QRegularExpression r("^\\[[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*(0?\\.[0-9]+)[[:space:]]*\\]$");
    return newVal.contains(r) == 1;
}

void tableFgClrPropEditor::setValue(QString newVal)
{
    QStringList v;
    if(this->isValidValue(newVal))
    {
        newVal.remove("[");
        newVal.remove("]");
        v = newVal.split(" ", QString::SkipEmptyParts);
        palette = this->ctrl->associatedWidget()->palette();
        palette.setColor(QPalette::WindowText, QColor::fromRgbF(v[0].toFloat(), v[1].toFloat(), v[2].toFloat()));
        this->ctrl->associatedWidget()->setPalette(palette);
    }
}

QString tableFgClrPropEditor::generateCode()
{
    QString ret;
    ret = "'ForegroundColor', " + this->getValue();
    return ret;
}

QWidget * tableFgClrPropEditor::getEditWidget()
{
    wdgClrChooser * wdgCh = new wdgClrChooser(this);
    wdgCh->setColor(this->getValue());
    connect(wdgCh->getLEColor(), SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    return wdgCh;
}

void tableFgClrPropEditor::valueChooserChanged()
{
    QLineEdit * ed;
    ed = (QLineEdit *) QObject::sender();
    disconnect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
    if(this->isValidValue(ed->text()))
    {
        this->setValue(ed->text());
    }
    else
    {

        ed->setText(this->getValue());
    }
    ed->selectAll();
    connect(ed, SIGNAL(editingFinished()), this, SLOT(valueChooserChanged()));
}


tableRearrangePropEditor::tableRearrangePropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
  this->value = false;
  this->propName = "RearrangeableColumns";
}

QString tableRearrangePropEditor::getValue()
{
    QString ret = "off";
    if(value){
        ret = "on";
    }
    return ret;
}

bool tableRearrangePropEditor::isValidValue(QString newVal)
{
    bool ret = ((newVal.toLower() == "on") ||
                (newVal.toLower() == "off"));
    return ret;
}

void tableRearrangePropEditor::setValue(QString newVal)
{
    if(newVal.toLower() == "on")
        this->value = true;
    else
        this->value = false;
}

QWidget * tableRearrangePropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("on");
    cb->addItem("off");
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    return cb;
}

void tableRearrangePropEditor::valueChooserChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    }
}

tableRowStripingPropEditor::tableRowStripingPropEditor(abstractUICCtrl *ctrl):abstractPropEditor(ctrl)
{
  this->value = false;
  this->propName = "RowStriping";
}

QString tableRowStripingPropEditor::getValue()
{
    QString ret = "off";
    if(value){
        ret = "on";
    }
    return ret;
}

bool tableRowStripingPropEditor::isValidValue(QString newVal)
{
    bool ret = ((newVal.toLower() == "on") ||
                (newVal.toLower() == "off"));
    return ret;
}

void tableRowStripingPropEditor::setValue(QString newVal)
{
    if(newVal.toLower() == "on")
        this->value = true;
    else
        this->value = false;
}

QWidget * tableRowStripingPropEditor::getEditWidget()
{
    comboUpdPropWdg * cb = new comboUpdPropWdg(this);
    cb->addItem("on");
    cb->addItem("off");
    cb->setCurrentIndex(cb->findText(this->getValue()));
    connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    return cb;
}

void tableRowStripingPropEditor::valueChooserChanged(QString text)
{
    comboUpdPropWdg * cb = (comboUpdPropWdg *)QObject::sender();
    if(this->isValidValue(text))
        this->setValue(text);
    else
    {
        disconnect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
        cb->setCurrentIndex(cb->findText(this->getValue()));
        connect(cb, SIGNAL(currentIndexChanged(QString)), this, SLOT(valueChooserChanged(QString)));
    }
}

unsigned int tableCtrl::uicNameCounter = 0;

unsigned int tableCtrl::getNameCounter()
{
    return uicNameCounter;
}

tableCtrl::tableCtrl(QWidget *parent, abstractUICCtrl *octaveParent, childWndDlg * parentWndDlg):abstractUICCtrl(parent, octaveParent, parentWndDlg)
{
    editableTable * table = new editableTable(parent, this);
    uicNameCounter++;
    this->setCtrlName("Table_"+QString::number(uicNameCounter));       

    this->setAssociatedWidget(table);
    this->adjTL->asociateCtrl(this->getAssociatedWidget());
    this->adjML->asociateCtrl(this->getAssociatedWidget());
    this->adjBL->asociateCtrl(this->getAssociatedWidget());
    this->adjTR->asociateCtrl(this->getAssociatedWidget());
    this->adjMR->asociateCtrl(this->getAssociatedWidget());
    this->adjBR->asociateCtrl(this->getAssociatedWidget());
    this->adjTM->asociateCtrl(this->getAssociatedWidget());
    this->adjBM->asociateCtrl(this->getAssociatedWidget());
    this->properties.registerProp(new fontNamePropEditor(this));
    this->properties.registerProp(new fontSizePropEditor(this));
    this->properties.registerProp(new fontWeightPropEditor(this));
    this->properties.registerProp(new fontAnglePropEditor(this));
    this->properties.registerProp(new toolTipPropEditor(this));
    this->properties.registerProp(new positionPropEditor(this));
    this->properties.registerProp(new tableFgClrPropEditor(this));
    this->properties.registerProp(new tableBgClrPropEditor(this));
    this->properties.registerProp(new tableRearrangePropEditor(this));
    this->properties.registerProp(new tableRowStripingPropEditor(this));
    this->properties.registerProp(new visiblePropEditor(this));
    this->setHaveCallBack(false);
}
void tableCtrl::setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd)
{
    editableTable * table = dynamic_cast<editableTable *>(this->associatedWidget());
    table->setParents(parent, this);

    this->adjTL->setParent(parent);
    this->adjML->setParent(parent);
    this->adjBL->setParent(parent);
    this->adjTR->setParent(parent);
    this->adjMR->setParent(parent);
    this->adjBR->setParent(parent);
    this->adjTM->setParent(parent);
    this->adjBM->setParent(parent);

    this->setOctaveParent(octaveParent);
    this->setParentWnd(parentWnd);
}

QStringList tableCtrl::generateMFile(QString path)
{
    QStringList ret;
    QString lineMCode;
    abstractPropEditor * prop;
    int i;
    ret.append("  " + this->properties.getValueByName("Name") + " = uitable( ...") ;
    if(getOctaveParent())
      ret.append("\t'parent'," + getOctaveParent()->getCtrlName());
    ret.append("\t'Units', 'pixels'");
    prop = this->properties.getFirst();
    while(prop)
    {
        if(prop->canGenerateCode())
        {
          lineMCode = "\t" + prop->generateCode();
          ret.append(lineMCode);
        }
        prop = this->properties.getNext();
    }
    for(i = 1; i < ret.count() - 1; i++)
        ret[i] = ret[i] + ", ... ";
    ret[i] = ret[i] + ");";
    if(this->haveCallBack())
      this->createCallBack(path);
    return ret;
}

QStringList tableCtrl::createCallBack(QString path __attribute__((unused)))
{
    QStringList toAdd;
    toAdd.append(this->preCallback());
    toAdd.append(this->srcCallBack);
    toAdd.append(this->posCallback());
    return toAdd;
}

abstractUICCtrl * tableCtrl::clone(QWidget *parent,
                                  abstractUICCtrl *octaveParent,
                                  childWndDlg * parentWnd){
    tableCtrl * ret = new tableCtrl(parent,
                                  octaveParent,
                                  parentWnd);
    abstractPropEditor *prop =  this->properties.getFirst();
    while(prop){
        ret->properties.getPropByName(prop->getPropName())->setValue(prop->getValue());
        prop = this->properties.getNext();
    }
    ret->srcCallBack = this->srcCallBack;
    return ret;
}
void tableCtrl::setHidden(bool isHidden){
    ((editableTable *)this->associatedWidget())->setHidden(isHidden);
}

tableCtrl::~tableCtrl()
{
  delete this->getAssociatedWidget();
}

tableCtrlGen::tableCtrlGen():controlGenerator("tableCtrl")
{
}

abstractUICCtrl * tableCtrlGen::getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg)
{
    QString src;
    tableCtrl * txt = new tableCtrl(parent->getAssociatedWidget(), parent, dlg);
    abstractPropEditor * prop;
    prop = txt->properties.getFirst();
    while(prop)
    {
      prop->setValue(xml.attributes().value(prop->getPropName()).toString());
      prop = txt->properties.getNext();
    }
    src = xml.readElementText();
    if(src.length() > 0)
      txt->setSrcCallBack(src.split('\n'));

    txt->deselect();
    return txt;

}


